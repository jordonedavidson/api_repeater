<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function index(Request $request, LoggerInterface $logger)
    {
        // $logger->info("post params: " . print_r($request->request->all(), true));
        // $logger->info($request->headers->__toString());

        /**
         * @var string The uri of the Origin
         *             For use in the Access-Control-Allow-Origin header
         */
        $origin = $request->headers->all('Origin')[0];

        /**
         * @var string The url of the API endpoint
         *             Required
         */
        $url = $request->request->get('url');

        /**
         * @var string The method to use for the API call
         *             Default value 'GET'
         */

        $method = $request->request->get('method');

        /**
         * @var string|object Body of a POST/PATCH/DELETE request to the API
         *                    Optional
         */
        $body = $request->request->get('body');

        /**
         * @var string The Content-Type to pass to the API call
         *             Optional
         */
        $contentType = $request->request->get('content_type');

        /**
         * @var array Any cookie strings to set in the request
         *            Optional
         */
        $setCookies = $request->request->get('cookies');

        /**
         * @var array The Headers array to send to the API
         */
        $headers = [];

        /**
         * Testing input variables
         */
        if (!in_array($method, ['GET', 'POST', 'PATCH', 'PUT', 'DELETE'])) {
            $method = 'GET';
        }

        /**
         * Set values in the $headers array for use in the client request.
         */
        // Content-type
        if (strlen($contentType) > 0) {
            $headers['Content-Type'] = $contentType;
        }
        // Cookies: Add cookies as an array indexed by 'Cookie'
        if (is_array($setCookies)) {
            foreach ($setCookies as $setMe) {
                $headers['Cookie'][] = $setMe;
            }
        }

        /**
         * @var Symfony\Component\HttpClient\HttpClient The client for making the API call.
         */
        $client = HttpClient::create();

        /**
         * @var Symfony\Component\HttpFoundation\Response The response from the API call
         *                                               to be passed back to the outside caller.
         */
        $response = new Response();

        /**
         * Setting the headers for the Reponse.
         */
        // Content-Type
        $response->headers->set('Content-Type', 'application/json');
        // CORS
        $response->headers->set('Access-Control-Allow-Origin', $origin);
        $response->headers->set('Access-Control-Allow-Credentials', 'true');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS');

        try {
            /** Make the API call */
            $apiResponse = $client->request(
                $method,
                $url,
                [
                    'headers' => $headers,
                    'body' => $body,
                ]
            );

            /** set the response */
            // Headers
            $responseHeaders = $apiResponse->getHeaders();

            // Content-Type
            $contentType =  $responseHeaders['content-type'][0];

            // Cookies (if any)
            $cookies = [];
            if (array_key_exists('set-cookie', $responseHeaders)) {
                foreach ($responseHeaders['set-cookie'] as $cookie) {
                    $cookies[] = $cookie;
                }
            }
            // Status-Code
            $response->setStatusCode($apiResponse->getStatusCode());
            // Body
            $body = '';
            $body = $apiResponse->getContent(false);

            $response->setContent(json_encode([
                'body' => $body,
                'content_type' => $contentType,
                'cookies' => $cookies
            ]));
        } catch (ClientException $e) {
            $logger->info('In ClientException');
            $response->headers->set('Content-Type', $apiResponse->getInfo('content_type'));
            $response->setContent($apiResponse->getContent(false));
            $response->setStatusCode($e->getCode());
        }

        // $logger->info("response from api endpoint");
        // $logger->info($response->__toString());

        /** Send back the response object */
        return $response;
    }
}
